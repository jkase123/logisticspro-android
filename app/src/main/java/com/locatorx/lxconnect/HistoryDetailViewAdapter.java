package com.locatorx.lxconnect;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import static com.locatorx.lxconnect.utils.JSONUtil.getSafeString;

public class HistoryDetailViewAdapter extends RecyclerView.Adapter<HistoryDetailViewAdapter.ViewHolder>
{
    private final List<Map<String,Object>> mValues;

    public HistoryDetailViewAdapter(List<Map<String,Object>> items)
    {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_history_detail_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        holder.mItem = mValues.get(position);
        holder.mLabel.setText(getSafeString(mValues.get(position), "label"));
        holder.mValue.setText(getSafeString(mValues.get(position), "value"));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final View mView;
        public final TextView mLabel;
        public final TextView mValue;
        public Map<String,Object> mItem;

        public ViewHolder(View view)
        {
            super(view);
            mView = view;
            mLabel = (TextView) view.findViewById(R.id.label);
            mValue = (TextView) view.findViewById(R.id.value);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mLabel.getText() + "'";
        }
    }
}
