package com.locatorx.lxconnect;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

public class WebFragment extends Fragment
{
    private final String BUNDLE_URL = "URL";

    WebView mWebView;
    String url;

    private MainActivityListener mListener;

    public WebFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    public void backPressed()
    {
        mWebView.goBack();
    }

    private String handlePromptMessage(String prompt)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String result = "done";

        if (prompt.equals("settings")) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.fromParts("package", "com.locatorx.lxconnect", null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (prompt.startsWith("enableBack")) {
            mListener.enableBackButton(true);
        } else if (prompt.startsWith("disableBack")) {
            mListener.enableBackButton(false);
        } else if (prompt.equals("scrollEnable")) {
        } else if (prompt.equals("scrollDisable")) {
        } else if (prompt.startsWith("get=")) {
            String parts[] = prompt.split("=");
            if (parts.length == 2)
                result = sharedPreferences.getString(parts[1], "false");
        } else if (prompt.startsWith("set=")) {
            String parts[] = prompt.split("=");
            if (parts.length == 3) {
                String key = parts[1];
                result = parts[2];
                sharedPreferences.edit().putString(key, result).commit();
            }
        } else if (prompt.startsWith("toggle=")) {
            String parts[] = prompt.split("=");
            if (parts.length == 2) {
                String key = parts[1];
                result = sharedPreferences.getString(key, "false");
                if (result.equals("false"))
                    result = "true";
                else
                    result = "false";
                sharedPreferences.edit().putString(key, result).commit();
            }
        }
        return result;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.fragment_web, container, false);

        // configure WebView
        //
        mWebView = layout.findViewById(R.id.webview);

        mWebView.clearCache(true);
        mWebView.clearHistory();
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
//        webSettings.setTextZoom(getTextZoomPercent());
        // Support http and https content
        webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsPrompt(WebView view, String url1, String message,
                                      String defaultValue, JsPromptResult jsResult) {
                String result = handlePromptMessage(message);
                jsResult.confirm(result);
                return true;
            }
        });

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

        });
        //

//        String url = WebFragmentArgs.fromBundle(getArguments()).getUrl();

        mWebView.loadUrl(url);

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mListener = (MainActivityListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
