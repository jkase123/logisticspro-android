package com.locatorx.lxconnect;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.locatorx.lxconnect.utils.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static com.locatorx.lxconnect.utils.JSONUtil.getSafeBooleanAlt;
import static com.locatorx.lxconnect.utils.JSONUtil.getSafeMap;
import static com.locatorx.lxconnect.utils.JSONUtil.getSafeMapArray;
import static com.locatorx.lxconnect.utils.JSONUtil.getSafeString;
import static com.locatorx.lxconnect.utils.JSONUtil.loadJSONData;
import static com.locatorx.lxconnect.utils.JSONUtil.saveJSONData;

public class FormFragment extends Fragment
{
    Button mSubmitButton;
    TextView mNumAssetsView;
    Spinner mActionSpinner;
    LinearLayout mFormLayout;
    LinearLayout mScanAssetsLayout;

    SimpleActionAdapter actionAdapter;

    List<Map<String,Object>> actionsList;

    Map<String,Object> mAsset;
    Map<String, Object> mAction;
    String mFormId;

    List<Map<String, Object>> scannedAssets;
    Set<String> scannedAssetIds;
    List<EditText> editTextList;
    List<Spinner> spinnerList;

    private MainActivityListener mListener;

    public FormFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        actionsList = new ArrayList<Map<String,Object>>();

        Map<String,Object> actions = loadJSONData(getActivity(), "actions");
        for (String key : actions.keySet()) {
            Map<String,Object> actionMap = getSafeMap(actions, key);
            if (!actionMap.containsKey("label")) {
                actionMap.put("label", key);
            }
            actionMap.put("action", key);
            actionsList.add(actionMap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.fragment_form, container, false);

        mActionSpinner = layout.findViewById(R.id.actionSpinner);

        mSubmitButton = layout.findViewById(R.id.submitButton);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                gatherFormDataAndSend();
//                buildFormForAction();
            }
        });

        mFormLayout = layout.findViewById(R.id.formFieldsLayout);

        return layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        mListener.enableProfileButton(true);
        mListener.enableBackButton(true);

        String assetString = FormFragmentArgs.fromBundle(getArguments()).getAsset();
        Map<String,Object> asset = JSONUtil.stringToJson(assetString);
        scanReceived(asset, false);
    }

    private void hideKeyboard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getRootView().getWindowToken(), 0);
    }

    private void gatherFormDataAndSend()
    {
        Map<String,Object> postBody = new HashMap<String,Object>();
        mListener.fillLatitudeLongitude(postBody);
        String assetId = getSafeString(mAsset, "assetId");
        postBody.put("action", getSafeString(mAction, "action"));
        Map<String,Object> propertiesMap = new HashMap<String,Object>();

        Map<String,Object> formDict = loadJSONData(getActivity(), "forms");
        Map<String,Object> form = getSafeMap(formDict, mFormId);
        if (form != null) {
            int iEditText = 0;
            int iSpinner = 0;

            propertiesMap.put("formId", mFormId);
            List<Map<String,Object>> formData = new ArrayList<Map<String,Object>>();
            List<Map<String, Object>> fields = getSafeMapArray(form, "fields");
            for (Map<String, Object> field : fields) {
                Map<String,Object> fieldDict = new HashMap<String,Object>();
                String fieldType = getSafeString(field, "fieldType");
                fieldDict.put("fieldType", fieldType);
                fieldDict.put("fieldKey", getSafeString(field, "fieldKey"));
                switch (fieldType) {
                    case "text":
                    {
                        EditText editText = editTextList.get(iEditText++);
                        String value = editText.getText().toString();
                        Log.i("text",value);
                        fieldDict.put("fieldValue", value);
                    }
                        break;
                    case "select":
                    {
                        Spinner spinner = spinnerList.get(iSpinner++);
                        SimpleActionAdapter selectAdapter = (SimpleActionAdapter)spinner.getAdapter();
                        int position = (Integer)spinner.getTag();
                        Map<String, Object> formOption = selectAdapter.getItem(position);
                        String value = getSafeString(formOption, "fieldValue");
                        Log.i("spinner", value);
                        fieldDict.put("fieldValue", value);
                    }
                        break;
                    case "scannedAssets":
                    {
                        fieldDict.put("fieldValues", scannedAssetIds);
                    }
                        break;
                }
                formData.add(fieldDict);
            }
            propertiesMap.put("formData", formData);
        }
        postBody.put("propertiesMap", propertiesMap);

        new ActionTask().execute(new ActionParams(mListener.getToken(), assetId, postBody));
    }

    private void processActionResults(Map<String,Object> map)
    {
        if (getSafeBooleanAlt(map, "success") && map.containsKey("json"))  {
            Map<String,Object> json = getSafeMap(map, "json");
            addActionToList(getSafeMap(json, "assetHistory"));

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Event Added");
            alertDialogBuilder.setMessage("Press Scan Next or Done");
            alertDialogBuilder.setPositiveButton("Scan Next",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            buildFormForAction();   // clean it up and get ready for the next
                            mListener.launchBarCodeActivity("", null, false, false);
                        }
                    });

            alertDialogBuilder.setNegativeButton("Done",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    mListener.goBack();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            Toast.makeText(getActivity(), "Error posting the form", Toast.LENGTH_SHORT).show();
        }
    }

    private void buildFormForAction()
    {
        mFormLayout.removeAllViewsInLayout();

        scannedAssets = new ArrayList<Map<String,Object>>();
        scannedAssetIds = new HashSet<String>();

        editTextList = new ArrayList<EditText>();
        spinnerList = new ArrayList<Spinner>();

        Log.i("***** buildFormForAction", "here");

        if (mFormId != null) {
            Map<String,Object> formDict = loadJSONData(getActivity(), "forms");
            Map<String,Object> form = getSafeMap(formDict, mFormId);
            if (form != null) {
                List<Map<String, Object>> fields = getSafeMapArray(form, "fields");
                for (Map<String,Object> field : fields) {
                    String label = getSafeString(field, "label");
                    switch (getSafeString(field, "fieldType")) {
                        case "text":
                        {
                            LinearLayout textLayout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.form_field_text, null);
                            TextView prompt = textLayout.findViewById(R.id.textFormFieldPrompt);
                            prompt.setText(label);
                            mFormLayout.addView(textLayout);

                            EditText editText = textLayout.findViewById(R.id.textFormFieldText);
                            editTextList.add(editText);
                            /*
                            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                @Override
                                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                    switch (actionId){
                                        case EditorInfo.IME_ACTION_DONE:
                                        case EditorInfo.IME_ACTION_NEXT:
                                        case EditorInfo.IME_ACTION_PREVIOUS:
                                            Log.i("editorAction", v.getText().toString());
                                            return true;
                                    }
                                    return false;
                                }
                            });
                            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    Log.i("editorAction", ((EditText)v).getText().toString());
                                }
                            });
                            */
                        }
                            break;
                        case "select":
                        {
                            LinearLayout selectLayout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.form_field_select, null);
                            TextView prompt = selectLayout.findViewById(R.id.selectFormFieldPrompt);
                            prompt.setText(label);
                            Spinner spinner = selectLayout.findViewById(R.id.selectSpinner);
                            List<Map<String,Object>> formOptions = getSafeMapArray(field, "formOptions");
                            SimpleActionAdapter selectAdapter = new SimpleActionAdapter(getActivity(), android.R.layout.simple_spinner_item, formOptions);
                            spinner.setAdapter(selectAdapter);
                            spinnerList.add(spinner);

                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                                    SimpleActionAdapter selectAdapter = (SimpleActionAdapter)adapterView.getAdapter();
                                    Map<String, Object> formOption = selectAdapter.getItem(position);
                                    adapterView.setTag(position);
                                    Log.i("spinner", "got it "+position);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapter) {
                                }
                            });
                            mFormLayout.addView(selectLayout);
                        }
                            break;
                        case "scannedAssets":
                        {
                            mScanAssetsLayout = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.form_field_scan_assets, null);
//                            TextView prompt = mScanAssetsLayout.findViewById(R.id.scanAssetsFormFieldPrompt);
//                            prompt.setText(label);
                            Button scanChildren = mScanAssetsLayout.findViewById(R.id.scanChildren);
                            final FormFragment formFragment = this;
                            scanChildren.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mListener.launchBarCodeActivity("", formFragment, true, false);
                                }
                            });
                            mNumAssetsView = mScanAssetsLayout.findViewById(R.id.numAssets);
                            mFormLayout.addView(mScanAssetsLayout);
                        }
                            break;
                    }
                }
            }
        }
    }

    public Map<String,Object> scanReceived(Map<String, Object> asset, boolean forChild)
    {
        Map<String,Object> result = new HashMap<String,Object>();
        try {
            if (asset != null) {
                if (!forChild) {
                    mAsset = asset;
                    mListener.showAssetTag(true, JSONUtil.getSafeString(asset, "tag"));

                    actionAdapter = new SimpleActionAdapter(getActivity(), R.layout.spinner_item, actionsList);
                    mActionSpinner.setAdapter(actionAdapter);
                    mActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                            mAction = actionAdapter.getItem(position);
                            mFormId = getSafeString(mAction, "formId");
                            buildFormForAction();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapter) {
                        }
                    });
                } else {
                    String assetId = getSafeString(asset, "assetId");
                    if (assetId.equals(getSafeString(mAsset, "assetId"))) {
                        result.put("message", "An Asset cannot be it\'s own parent");
                        result.put("error", true);
                        return result;
                    }
                    if (scannedAssetIds.contains(assetId)) {
                        result.put("message", "Asset is already in the list");
                        result.put("error", true);
                        return result;
                    }
                    scannedAssetIds.add(assetId);
                    scannedAssets.add(asset);

                    mNumAssetsView.setText(scannedAssetIds.size() + " added");

                    ((MainActivity)getActivity()).childScanCount = "Total Added: "+scannedAssetIds.size();

                    RelativeLayout assetLayout = (RelativeLayout) LayoutInflater.from(getActivity()).inflate(R.layout.form_field_asset, null);
                    TextView assetTagView = assetLayout.findViewById(R.id.childAssetTag);
                    assetTagView.setText(getSafeString(asset, "tag"));
                    mScanAssetsLayout.addView(assetLayout);

                    result.put("message", "Add Another Asset");
                    result.put("success", true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("scanReceived", "error: "+e);
            result.put("message", "Unknown Error");
            result.put("error", true);
        }

        return result;
    }

    private void addActionToList(Map<String,Object> assetHistory)
    {
        if (assetHistory == null)
            return;

        // the assetHistory map is stored with the timeOfLog as the key (for ease of sorting using a TreeMap)
        //
        Map<String,Object> assetHistories = loadJSONData(getActivity(), "assetHistories");
        assetHistories.put(getSafeString(assetHistory, "timeOfLog"), assetHistory);
        if (assetHistories.size() > 25) {
            TreeMap<String,Object> sortedMap = new TreeMap<String,Object>(assetHistories);
            String key = sortedMap.firstKey();
            sortedMap.remove(key);
            saveJSONData(getActivity(), "assetHistories", sortedMap);
        } else
            saveJSONData(getActivity(), "assetHistories", assetHistories);
    }

    public class SimpleActionAdapter extends ArrayAdapter<Map<String,Object>>
    {
        private Context context;
        private List<Map<String,Object>> values;

        public SimpleActionAdapter(Context context, int textViewResourceId, List<Map<String,Object>> values)
        {
            super(context, textViewResourceId, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount()
        {
            return values.size();
        }

        @Override
        public Map<String,Object> getItem(int position)
        {
            return values.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            TextView label = (TextView) super.getView(position, convertView, parent);
            label.setTextColor(Color.BLACK);
            Map<String,Object> action = values.get(position);
            label.setText(getSafeString(action, "label"));

            return label;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            TextView label = (TextView) super.getDropDownView(position, convertView, parent);
            label.setTextColor(Color.BLACK);
            Map<String,Object> action = values.get(position);
            label.setText(getSafeString(action, "label"));

            return label;
        }
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mListener = (MainActivityListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    class ActionParams
    {
        String token, assetId;
        Map<String,Object> postBody;

        public ActionParams(String token, String assetId, Map<String,Object> postBody)
        {
            this.token = token;
            this.assetId = assetId;
            this.postBody = postBody;
        }
    }

    private class ActionTask extends AsyncTask<ActionParams, Void, Map<String, Object>>
    {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(getActivity(), "", "Loading", true,
                    false); // Create and show Progress dialog
        }

        @Override
        protected Map<String, Object> doInBackground(ActionParams... params)
        {
            try {
                ActionParams actionParams = params[0];
                return AppServerClient.processActionFromApp(actionParams.token, actionParams.assetId, actionParams.postBody);

            } catch (Exception e) {
                Map<String, Object> errorMap = new HashMap<String, Object>();
                errorMap.put("error", e);
                return errorMap;
            }
        }

        @Override
        protected void onPostExecute(Map<String, Object> map)
        {
            pd.dismiss();
            try {
                processActionResults(map);
            } catch (Exception e) {}
        }
    }

}
