package com.locatorx.lxconnect;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.barcode.Barcode;
import com.locatorx.lxconnect.barCodes.BarcodeReaderActivity;
import com.locatorx.lxconnect.login.LoginActivity;
import com.locatorx.lxconnect.utils.JSONUtil;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.locatorx.lxconnect.utils.JSONUtil.jsonToString;
import static com.locatorx.lxconnect.utils.JSONUtil.saveJSONData;

public class MainActivity extends AppCompatActivity implements MainActivityListener
{
    private static final int BARCODE_READER_ACTIVITY_REQUEST = 1208;

//    public static final int kScanModeBoth = 0;
    public static final int kScanModeQR = 1;
//    public static final int kScanModeNFC = 2;

    private final String BUNDLE_HAS_QR =    "HAS_QR";
    private final String BUNDLE_UUID =      "UUID";
    private final String BUNDLE_RAW_URL =   "RAW_URL";
    private final String BUNDLE_SCAN_MODE = "SCAN_MODE";
    public static final String BUNDLE_TOKEN =      "TOKEN";
    public static final String BUNDLE_APP_USER_ID = "APP_USER_ID";

    boolean writeMode;

    TextView mAssetTag;
    ImageView mLocatorX;
    Vibrator myVib;
    MediaPlayer mp;
    ImageView mButtonBack;
    ImageView mProfileButton;

    public String token, appUserId;
    public String childScanCount = "Total Child Assets: 0";
    boolean scanForChild = false;
    NavController mNavController = null;
    private FormFragment mFormFragment;

    private FusedLocationProviderClient fusedLocationClient;

    int iScanMode = kScanModeQR;
    boolean hasQR = false;
    private String barcodeUrl, assetUUID, qrHash;

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mNavController = Navigation.findNavController(this, R.id.nav_host_fragment);

        mAssetTag = findViewById(R.id.assetTag);
        mLocatorX = findViewById(R.id.locatorx);
        mButtonBack = findViewById(R.id.btn_back);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNavController.popBackStack();
            }
        });
        mProfileButton = findViewById(R.id.btn_profile);
        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProfilePopup(v);
            }
        });

        final Intent intent = getIntent();
        if (intent != null) {
            token = intent.getStringExtra(BUNDLE_TOKEN);
            appUserId = intent.getStringExtra(BUNDLE_APP_USER_ID);
        }

        // initialize Location services
        //
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();

        locationCallback = new LocationCallback() {
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    currentLocation = location;
                    Log.i("MainActivity*****", "Location 1: "+location.getLatitude()+", "+location.getLongitude());
                }
            };
        };

        // sound and vibration
        //
        myVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        mp = MediaPlayer.create(this, R.raw.chime);
    }

    public void goBack()
    {
        mNavController.popBackStack();
    }

//    private final String kFAQUrl = "";
//    private final String kTermsUrl = "https://wiki.locatorx.com/terms-of-use";
//    private final String kLocatorXUrl = "https://www.locatorx.com";

    private void showProfilePopup(View view)
    {
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.profile_popup, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height-48, focusable);

        //Set the location of the window on the screen
        popupWindow.showAsDropDown(view, 0, 48);

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        TextView btnHistory = popupView.findViewById(R.id.btn_scanHistory);
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDestination nd = mNavController.getCurrentDestination();
                String label = (String) nd.getLabel();
                if (label.equals("fragment_scan")) {
                    mNavController.navigate(ScanFragmentDirections.actionScanFragmentToHistoryFragment());
                } if (label.equals("fragment_form")) {
                    mNavController.navigate(FormFragmentDirections.actionFormFragmentToHistoryFragment());
                }
                popupWindow.dismiss();
            }
        });
        TextView btnLogout = popupView.findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleLogout();
                popupWindow.dismiss();
            }
        });
        /*
        TextView btnFAQ = popupView.findViewById(R.id.btn_FAQ);
        btnFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDestination nd = mNavController.getCurrentDestination();
                String label = (String) nd.getLabel();
                if (label.equals("fragment_scan")) {
                    mNavController.navigate(ScanFragmentDirections.actionScanFragmentToWebFragment(kFAQUrl));
                } if (label.equals("fragment_form")) {
                    mNavController.navigate(FormFragmentDirections.actionFormFragmentToWebFragment(kFAQUrl));
                }
                popupWindow.dismiss();
            }
        });
        TextView btnTerms = popupView.findViewById(R.id.btn_termsAndConditions);
        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDestination nd = mNavController.getCurrentDestination();
                String label = (String) nd.getLabel();
                if (label.equals("fragment_scan")) {
                    mNavController.navigate(ScanFragmentDirections.actionScanFragmentToWebFragment(kTermsUrl));
                } if (label.equals("fragment_form")) {
                    mNavController.navigate(FormFragmentDirections.actionFormFragmentToWebFragment(kTermsUrl));
                }
                popupWindow.dismiss();
            }
        });
        TextView btnLocatorX = popupView.findViewById(R.id.btn_locatorX);
        btnLocatorX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        */

        final Switch swSound = popupView.findViewById(R.id.sw_sound);
        String result = sharedPreferences.getString("sound", "true");
        swSound.setChecked(result.equals("true"));
        swSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String result = sharedPreferences.getString("sound", "true");
                result = (result.equals("false") ? "true" : "false");
                sharedPreferences.edit().putString("sound", result).commit();
            }
        });
        final Switch swVibrate = popupView.findViewById(R.id.sw_vibration);
        result = sharedPreferences.getString("vibrate", "true");
        swVibrate.setChecked(result.equals("true"));
        swVibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String result = sharedPreferences.getString("vibrate", "true");
                result = (result.equals("false") ? "true" : "false");
                sharedPreferences.edit().putString("vibrate", result).commit();
            }
        });
        /*
        final Switch swHistory = popupView.findViewById(R.id.sw_history);
        result = sharedPreferences.getString("history", "true");
        swHistory.setChecked(result.equals("true"));
        swHistory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String result = sharedPreferences.getString("history", "true");
                result = (result.equals("false") ? "true" : "false");
                sharedPreferences.edit().putString("history", result).commit();
            }
        });
         */

        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void showAssetTag(boolean showTag, String tagOverride)
    {
        if (showTag) {
            mAssetTag.setText(tagOverride);
            mAssetTag.setVisibility(View.VISIBLE);
            mLocatorX.setVisibility(View.GONE);
        } else {
            mAssetTag.setVisibility(View.GONE);
            mLocatorX.setVisibility(View.VISIBLE);
        }
    }

    // SettingsFragment.OnSettingsFragmentListener
    //
    public void handleLogout()
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().remove("email")
                .remove("password").commit();

        Intent activityIntent = new Intent(this, LoginActivity.class);
        startActivity(activityIntent);

        setResult(Activity.RESULT_OK);

        finish();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        startLocationupdates();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        setIntent(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        Log.i("MainActivity*****", "onSave");

        if (assetUUID != null)
            outState.putString(BUNDLE_UUID, assetUUID);
        if (barcodeUrl != null)
            outState.putString(BUNDLE_RAW_URL, barcodeUrl);

        outState.putInt(BUNDLE_HAS_QR, (hasQR ? 1 : 0));
        outState.putInt(BUNDLE_SCAN_MODE, iScanMode);
        if (appUserId != null)
            outState.putString(BUNDLE_APP_USER_ID, appUserId);
        outState.putString(BUNDLE_TOKEN, token);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        Log.i("MainActivity*****", "onRestore");

        super.onRestoreInstanceState(savedInstanceState);

        assetUUID = savedInstanceState.getString(BUNDLE_UUID);
        barcodeUrl = savedInstanceState.getString(BUNDLE_RAW_URL);
        iScanMode = savedInstanceState.getInt(BUNDLE_SCAN_MODE);
        hasQR = savedInstanceState.getInt(BUNDLE_HAS_QR) == 1;
        token = savedInstanceState.getString(BUNDLE_TOKEN);
        appUserId = savedInstanceState.getString(BUNDLE_APP_USER_ID);
    }

    public void onListFragmentInteraction(Map<String,Object> item)
    {
        HistoryFragmentDirections.ActionHistoryFragmentToHistoryDetailFragment action =
                HistoryFragmentDirections.actionHistoryFragmentToHistoryDetailFragment(JSONUtil.jsonToString(item));
        mNavController.navigate(action);
    }

    private Location currentLocation = null;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private final int kPermissionsFineLocation = 1000;

    private void startLocationupdates()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PERMISSION_GRANTED) {
            Log.i("MainActivity", "Location Permission Not Granted");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        kPermissionsFineLocation);
            }
        } else {

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                currentLocation = location;
                                Log.i("MainActivity", "Location 2: "+location.getLatitude()+", "+location.getLongitude());
                            }
                        }
                    });

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case kPermissionsFineLocation: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationupdates();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    // SettingsFragment.OnSettingsFragmentListener
    //
    public void enableBackButton(boolean enable)
    {
        mButtonBack.setVisibility((enable ? View.VISIBLE : View.GONE));
    }

    public void enableProfileButton(boolean enable)
    {
        mProfileButton.setVisibility((enable ? View.VISIBLE : View.GONE));
    }

    // ScanFragment.OnScanFragmentListener
    //
    public void launchBarCodeActivity(String scanText, FormFragment formFragment, boolean forChild, boolean needCountdown)
    {
        if (formFragment != null)
            mFormFragment = formFragment;

        scanForChild = forChild;
        Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false, forChild, scanText, childScanCount, needCountdown);
        startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
//            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
//            Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
            barcodeUrl = barcode.rawValue;
            assetUUID = null;
            qrHash = null;
            parseQRScan();
        }

    }

    private void parseQRScan()
    {
        try {
            URL url = new URL(barcodeUrl);
            String params[] = url.getQuery().split("&");
            for (String param : params) {
                String tagComponents[] = param.split("=");
                switch (tagComponents[0]) {
                    case "assetId":
                        assetUUID = tagComponents[1];
                        break;
                    case "hash":
                        qrHash = tagComponents[1];
                        break;
                }
            }
            if (assetUUID == null) {
                String parts[] = url.getPath().split("/");
                for (String part : parts) {
                    if (part.length() == 36) {
                        try {
                            UUID uuid = UUID.fromString(part);  // make sure it's a UUID
                            assetUUID = part;
                            break;
                        } catch (Exception e) {

                        }
                    }
                }
            }

            hasQR = true;

        } catch (Exception e) {

        }

        fetchScanResults();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.getString("sound", "false").equals("true"))
            mp.start();

        if (sharedPreferences.getString("vibrate", "false").equals("true"))
            myVib.vibrate(50);

    }

    private void fetchScanResults()
    {
        if (currentLocation == null) {
            Toast.makeText(this,"No Location information Available", Toast.LENGTH_SHORT).show();
            return;
        }
        new ScanTask().execute(new ScanParams(assetUUID, qrHash, null /*nfcText*/, currentLocation.getLatitude(), currentLocation.getLongitude()));
    }

    public void fillLatitudeLongitude(Map<String,Object> postBody)
    {
        if (currentLocation == null) {
            Toast.makeText(this,"No Location information Available", Toast.LENGTH_SHORT).show();
            return;
        }
        postBody.put("latitude", currentLocation.getLatitude());
        postBody.put("longitude", currentLocation.getLongitude());
    }

    public String getToken()
    {
        return token;
    }

    private void processScanResults(Map<String, Object> map)
    {
        hasQR = false;
        assetUUID = barcodeUrl = null;

        Map<String, Object> json = JSONUtil.getSafeMap(map, "json");
        Log.i("MainActivity*****", "json: " + json);
        if (JSONUtil.getSafeBooleanAlt(map, "success") && json.containsKey("asset")) {
            Map<String, Object> asset = JSONUtil.getSafeMap(json, "asset");
            showSecurityElements(JSONUtil.getSafeBooleanAlt(json, "isCounterfeit"),
                    JSONUtil.getSafeBooleanAlt(json, "missingQR"),
                    JSONUtil.getSafeBooleanAlt(json, "missingNFC"),
                    JSONUtil.getSafeBooleanAlt(json, "nfcValid"));
            if (!scanForChild) {
                saveJSONData(this, "asset", asset);

                ScanFragmentDirections.ActionScanFragmentToFormFragment action =
                        ScanFragmentDirections.actionScanFragmentToFormFragment(JSONUtil.jsonToString(asset));
                mNavController.navigate(action);
            } else {
//                FormFragment formFragment = (FormFragment)getSupportFragmentManager().findFragmentById(R.id.formFragment);
                Map<String,Object> scanResult = mFormFragment.scanReceived(asset, true);
                if (scanResult != null)
                    launchBarCodeActivity(jsonToString(scanResult), null, true, true);
            }
        }
    }

    LinearLayout securityOverLay;

    private void showSecurityBanner(boolean ok, boolean hidden, boolean fade)
    {
//        final View v = (ok ? mSecurityBackgroundGreen : mSecurityBackgroundRed);
        securityOverLay = (LinearLayout) LayoutInflater.from(this).inflate((ok ? R.layout.security_green : R.layout.security_red), null);

        securityOverLay.setAlpha(0f);
        securityOverLay.setVisibility(View.VISIBLE);

        final RelativeLayout topHeader = findViewById(R.id.topHeader);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.LayoutParams.m, topHeader.getId());
//        params.setMargins(0, 20, 0, 0);
        topHeader.addView(securityOverLay, params);

        securityOverLay.animate()
                .alpha(1f)
                .setDuration(1500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        securityOverLay.animate()
                                .alpha(0f)
                                .setDuration(1500)
                                .setStartDelay(1500)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
//                                        v.setVisibility(View.GONE);
                                        topHeader.removeView(securityOverLay);
                                    }
                                });
                    }
                });
    }

    private void showSecurityElements(boolean isCounterfeit, boolean missingQR, boolean missingNFC, boolean nfcValid)
    {
        boolean valid = true;

        if (isCounterfeit) {
            valid = false;
        } else {
            if (nfcValid) {
                if (missingQR /* && iScanMode != kScanModeNFC */) {
                    valid = false;
                }
            }
            if (missingNFC && iScanMode != kScanModeQR) {
                valid = false;
            }
        }
        if (!missingNFC && !nfcValid) {
            valid = false;
        }
        if (!valid)
            showSecurityBanner(valid, false, true);
    }

    class ScanParams
    {
        String assetId, qrHash, nfcString;
        Double latitude, longitude;

        public ScanParams(String assetId, String qrHash, String nfcString, Double latitude, Double longitude)
        {
            this.assetId = assetId;
            this.qrHash = qrHash;
            this.nfcString = nfcString;
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    private class ScanTask extends AsyncTask<ScanParams, Void, Map<String, Object>> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(MainActivity.this, "", "Loading", true,
                    false); // Create and show Progress dialog
        }

        @Override
        protected Map<String, Object> doInBackground(ScanParams... params) {

            try {
                ScanParams scanParams = params[0];
                return AppServerClient.processScanFromApp(token, scanParams.assetId, scanParams.qrHash, scanParams.nfcString, scanParams.latitude, scanParams.longitude);

            } catch (Exception e) {
                Map<String, Object> errorMap = new HashMap<String, Object>();
                errorMap.put("error", e);
                return errorMap;
            }
        }

        @Override
        protected void onPostExecute(Map<String, Object> map) {
            pd.dismiss();
            try {
                processScanResults(map);
            } catch (Exception e) {}
        }
    }

}
