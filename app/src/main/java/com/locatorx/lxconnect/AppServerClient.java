package com.locatorx.lxconnect;

import com.locatorx.lxconnect.utils.JSONUtil;

import java.util.HashMap;
import java.util.Map;

import static com.locatorx.lxconnect.utils.JSONUtil.fetchJson;

public class AppServerClient
{
    private static final boolean STAGING = BuildConfig.STAGING;

    public static String getCSMToken()
    {
        if (STAGING) {
            return "62418f50-44e9-4173-aaeb-0521f8d64491";
        }
        return "a90c7ec1-f45e-4807-9539-604096adf41d";
    }

    public static String getUrlBase()
    {
        if (STAGING) {
            return "https://api-staging.locatorx.com/lx-atlas/api/";
        }
        return "https://api.locatorx.com/lx-atlas/api/";
    }

    public static String getSettingsPageUrl()
    {
        if (STAGING) {
            return "https://lxconnect-staging.locatorx.com/settings";
        }
        return "https://lxconnect.locatorx.com/settings";
    }

    public static Map<String,Object> fetchNFCString(String url)
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            return fetchJson(url, null, null);

        } catch (Exception e) {
        }

        return result;
    }

    public static Map<String,Object> processScanFromApp(String token, String assetId, String qrHash, String nfcString, Double latitude, Double longitude)
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            String url = null;
            Map<String,Object> postBody = new HashMap<String,Object>();
            if (assetId != null) {
                url = getUrlBase() + "assets/"+assetId+"/scanFromApp";
            }

            Map<String,String> headerMap = new HashMap<String,String>();
            headerMap.put("auth-token", token);

            if (nfcString != null) {
                postBody.put("nfcString", nfcString);
                if (url == null)
                    url = getUrlBase() + "assets/csm/mobile/scanFromAppNFC";
                headerMap.put("CSM", getCSMToken());
            }
            postBody.put("latitude", latitude);
            postBody.put("longitude", longitude);
            if (qrHash != null)
                postBody.put("qrHash", qrHash);

            return fetchJson(url, postBody, headerMap);

        } catch (Exception e) {
        }

        return result;
    }

    public static Map<String,Object> processActionFromApp(String token, String assetId, Map<String,Object> postBody)
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            String url = getUrlBase() + "assets/"+assetId+"/action/";
            Map<String,String> headerMap = new HashMap<String,String>();
            headerMap.put("auth-token", token);

            return fetchJson(url, postBody, headerMap);

        } catch (Exception e) {
        }

        return result;
    }

    public static Map<String,Object> processAppSettings()
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            String url = getUrlBase() + "appSettings/mobile/LXConnect";

            Map<String,String> headerMap = new HashMap<String,String>();
            headerMap.put("CSM", getCSMToken());

            return fetchJson(url, null, headerMap);

        } catch (Exception e) {
        }

        return result;
    }

    public static Map<String,Object> processLogin(String email, String password)
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            String url = null;
            Map<String,Object> postBody = new HashMap<String,Object>();
            url = getUrlBase() + "auth/mobile";

            if (email != null && password != null) {
                postBody.put("email", email);
                postBody.put("password", password);
            }

            Map<String,String> headerMap = new HashMap<String,String>();
            headerMap.put("CSM", getCSMToken());

            return fetchJson(url, postBody, headerMap);

        } catch (Exception e) {
        }

        return result;
    }

}
