package com.locatorx.lxconnect;

import java.util.Map;

public interface MainActivityListener
{
    void onListFragmentInteraction(Map<String, Object> item);
    void launchBarCodeActivity(String scanText, FormFragment formFragment, boolean forChild, boolean needsCountdown);
    void fillLatitudeLongitude(Map<String,Object> postBody);
    void showAssetTag(boolean showTag, String tagOverride);
    String getToken();
    void goBack();
    void enableBackButton(boolean enable);
    void enableProfileButton(boolean enable);
}
