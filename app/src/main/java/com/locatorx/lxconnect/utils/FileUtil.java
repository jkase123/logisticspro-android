package com.locatorx.lxconnect.utils;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil
{
    public static String readFileToString(String path)
    {
        try {
            byte encoded[] = Files.readAllBytes(Paths.get(path));
            return new String(encoded, StandardCharsets.UTF_8);

        } catch (Exception e) {
        }
        return "";
    }

    public static byte[] readFileToBytes(String path)
    {
        try {
            return Files.readAllBytes(Paths.get(path));

        } catch (Exception e) {
        }
        return null;
    }

    public static boolean writeStringToFile(String path, String text)
    {
        try {
            Files.write(Paths.get(path), text.getBytes());
            return true;

        } catch (Exception e) {
        }
        return false;
    }

    public static boolean writeBytesToFile(String path, byte data[])
    {
        try {
            Files.write(Paths.get(path), data);
            return true;

        } catch (Exception e) {
        }
        return false;
    }

}
