package com.locatorx.lxconnect;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.locatorx.lxconnect.utils.JSONUtil;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import static com.locatorx.lxconnect.utils.JSONUtil.getSafeMap;
import static com.locatorx.lxconnect.utils.JSONUtil.loadJSONData;

public class HistoryFragment extends Fragment
{
    private static final String ARG_COLUMN_COUNT = "column-count";

    private MainActivityListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HistoryFragment() {
    }

    public static HistoryFragment newInstance(int columnCount)
    {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.fragment_history, container, false);
        View listView = layout.findViewById(R.id.list);

        // Set the adapter
        if (listView instanceof RecyclerView) {
            Context context = layout.getContext();
            RecyclerView recyclerView = (RecyclerView) listView;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            Map<String,Object> assetHistories = loadJSONData(getActivity(), "assetHistories");
            DateTimeFormatter dfIn
                    = DateTimeFormatter.ofPattern("yyyyMMddHHmmss", Locale.ENGLISH);
            DateTimeFormatter dfOut
                    = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH);
            for (String key : assetHistories.keySet()) {
                Map<String,Object> map = JSONUtil.getSafeMap(assetHistories, key);
                String formattedDate = LocalDateTime.parse(key, dfIn)
                        .atOffset(ZoneOffset.UTC)
                        .atZoneSameInstant(ZoneId.systemDefault())
                        .format(dfOut);
                map.put("timeOfLogString", formattedDate);
            }
            TreeMap<String,Object> sortedMap = new TreeMap<String,Object>(assetHistories);
            List<Map<String,Object>> sortedList = new ArrayList<Map<String,Object>>();
            for (String key : sortedMap.descendingKeySet()) {
                sortedList.add(getSafeMap(sortedMap, key));
            }

            recyclerView.setAdapter(new HistoryViewAdapter(sortedList, mListener));
        }
        return layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        mListener.showAssetTag(true, "Asset History");
        mListener.enableProfileButton(false);
        mListener.enableBackButton(true);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mListener = (MainActivityListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
