package com.locatorx.lxconnect;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import static com.locatorx.lxconnect.utils.JSONUtil.getSafeString;

public class HistoryViewAdapter extends RecyclerView.Adapter<HistoryViewAdapter.ViewHolder>
{
    private final List<Map<String,Object>> mValues;
    private final MainActivityListener mListener;

    public HistoryViewAdapter(List<Map<String,Object>> items, MainActivityListener listener)
    {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        holder.mItem = mValues.get(position);
        holder.mTimeOfLog.setText(getSafeString(mValues.get(position), "timeOfLogString"));
        holder.mAssetTag.setText(getSafeString(mValues.get(position), "assetTag"));
        String event = getSafeString(mValues.get(position), "event");
        if (event == null)
            event = getSafeString(mValues.get(position), "action");
        holder.mAction.setText(event);
        holder.mCity.setText(getSafeString(mValues.get(position), "city"));
        holder.mState.setText(getSafeString(mValues.get(position), "state"));
        holder.mCountry.setText(getSafeString(mValues.get(position), "country"));
        holder.mPostalCode.setText(getSafeString(mValues.get(position), "postalCode"));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public final View mView;
        public final TextView mTimeOfLog;
        public final TextView mAssetTag;
        public final TextView mAction;
        public final TextView mCity;
        public final TextView mState;
        public final TextView mCountry;
        public final TextView mPostalCode;
        public Map<String,Object> mItem;

        public ViewHolder(View view)
        {
            super(view);
            mView = view;
            mTimeOfLog = (TextView) view.findViewById(R.id.timeOfLog);
            mAssetTag = (TextView) view.findViewById(R.id.assetTag);
            mAction = (TextView) view.findViewById(R.id.action);
            mCity = (TextView) view.findViewById(R.id.city);
            mState = (TextView) view.findViewById(R.id.state);
            mCountry = (TextView) view.findViewById(R.id.country);
            mPostalCode = (TextView) view.findViewById(R.id.postalCode);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mAssetTag.getText() + "'";
        }
    }
}
