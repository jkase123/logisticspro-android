package com.locatorx.lxconnect;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.locatorx.lxconnect.utils.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.locatorx.lxconnect.utils.JSONUtil.getSafeDouble;
import static com.locatorx.lxconnect.utils.JSONUtil.getSafeMap;
import static com.locatorx.lxconnect.utils.JSONUtil.getSafeString;
import static com.locatorx.lxconnect.utils.JSONUtil.getSafeStringAlt;

public class HistoryDetailFragment extends Fragment
{
    public Map<String,Object> mItem;

    private MainActivityListener mListener;

    public HistoryDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        /*
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.fragment_history_detail, container, false);
        View listView = layout.findViewById(R.id.detailList);

        String itemString = HistoryDetailFragmentArgs.fromBundle(getArguments()).getEvent();
        mItem = JSONUtil.stringToJson(itemString);
        TextView assetTag = layout.findViewById(R.id.assetTag);
        assetTag.setText(getSafeStringAlt(mItem, "assetTag"));

        if (listView instanceof RecyclerView) {
            Context context = layout.getContext();
            RecyclerView recyclerView = (RecyclerView) listView;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
            Map<String,Object> locationMap = new HashMap<String,Object>();
            locationMap.put("label", "LOCATION");
            String location = getSafeStringAlt(mItem, "facility");
            if (mItem.containsKey("latitude")) {
                if (location.length() > 0)
                    location += "\n";
                location += getSafeDouble(mItem, "latitude")+", "+getSafeDouble(mItem, "longitude");
            }
            if (mItem.containsKey("city") || mItem.containsKey("state") || mItem.containsKey("postalCode") || mItem.containsKey("country")) {
                if (location.length() > 0)
                    location += "\n";
                if (mItem.containsKey("city"))
                    location += getSafeStringAlt(mItem, "city") + ", " + getSafeStringAlt(mItem, "state") + " "+ getSafeStringAlt(mItem, "postalCode");
                if (mItem.containsKey("country"))
                    location += " " + getSafeStringAlt(mItem, "country");
            }
            locationMap.put("value", location);
            dataList.add(locationMap);
            //
            Map<String,Object> dateTimeMap = new HashMap<String,Object>();
            dateTimeMap.put("label", "DATE/TIME");
            dateTimeMap.put("value", getSafeString(mItem, "timeOfLogString"));
            dataList.add(dateTimeMap);
            //
            Map<String,Object> eventMap = new HashMap<String,Object>();
            eventMap.put("label", "EVENT");
            String event = getSafeString(mItem, "event");
            if (event == null)
                event = getSafeString(mItem, "action");

            eventMap.put("value", event);
            dataList.add(eventMap);
            //
            Map<String,Object> unitMap = new HashMap<String,Object>();
            unitMap.put("label", "UNIT");
            unitMap.put("value", getSafeStringAlt(mItem, "assetType"));
            dataList.add(unitMap);
            //
            Map<String,Object> properties = getSafeMap(mItem, "propertiesMap");
            for (String key : properties.keySet()) {
                if (!key.equals("formData") && !key.equals("formId")) {
                    Object stringValue = properties.get(key);
                    if (stringValue instanceof String) {
                        Map<String,Object> propMap = new HashMap<String,Object>();
                        propMap.put("label", key);
                        propMap.put("value", stringValue);
                        dataList.add(propMap);
                    }
                }
            }

            recyclerView.setAdapter(new HistoryDetailViewAdapter(dataList));
        }
        return layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        mListener.showAssetTag(true, "Asset History Detail");
        mListener.enableProfileButton(false);
        mListener.enableBackButton(true);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mListener = (MainActivityListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnHistoryDetailInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

}
