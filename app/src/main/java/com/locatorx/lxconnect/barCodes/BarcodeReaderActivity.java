package com.locatorx.lxconnect.barCodes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Layout;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.vision.barcode.Barcode;
import com.locatorx.lxconnect.R;
import com.locatorx.lxconnect.utils.JSONUtil;

import java.util.List;
import java.util.Map;

public class BarcodeReaderActivity extends AppCompatActivity implements BarcodeReaderFragment.BarcodeReaderListener
{
    public static String KEY_CAPTURED_BARCODE = "key_captured_barcode";
    public static String KEY_CAPTURED_RAW_BARCODE = "key_captured_raw_barcode";

    private static final String KEY_AUTO_FOCUS = "key_auto_focus";
    private static final String KEY_USE_FLASH = "key_use_flash";
    private static final String KEY_FOR_CHILD = "key_for_child";
    private static final String KEY_SCAN_TEXT = "key_scan_text";
    private static final String KEY_CHILD_SCAN_COUNT_TEXT = "child_scan_count_text";
    private static final String KEY_NEED_COUNTDOWN = "need_countdown";

    private boolean autoFocus = false;
    private boolean useFlash = false;
    private boolean forChild = false;
    private boolean needCountdown = false;
    private String scanText, childScanCountText;

    private BarcodeReaderFragment mBarcodeReaderFragment;
    private ImageView mCloseButton;
    private TextView mStatusText, mChildCount;
    private Button mCountdown;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_reader);

        final Intent intent = getIntent();
        if (intent != null) {
            autoFocus = intent.getBooleanExtra(KEY_AUTO_FOCUS, false);
            useFlash = intent.getBooleanExtra(KEY_USE_FLASH, false);
            forChild = intent.getBooleanExtra(KEY_FOR_CHILD, false);
            scanText = intent.getStringExtra(KEY_SCAN_TEXT);
            childScanCountText = intent.getStringExtra(KEY_CHILD_SCAN_COUNT_TEXT);
            needCountdown = intent.getBooleanExtra(KEY_NEED_COUNTDOWN, false);
        }

        mCloseButton = findViewById(R.id.closeScan);
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mStatusText = findViewById(R.id.barcodeStatus);
        RelativeLayout messageBackground = findViewById(R.id.barcodeLayout);
        if (scanText != null && scanText.length() > 0) {
            Map<String,Object> scanResult = JSONUtil.stringToJson(scanText);
            mStatusText.setText(JSONUtil.getSafeStringAlt(scanResult, "message"));
            if (scanResult.containsKey("success")) {
                mStatusText.setTextColor(ContextCompat.getColor(this, R.color.black));
                messageBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.text1Color));
            } else {
                mStatusText.setTextColor(ContextCompat.getColor(this, R.color.white));
                messageBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.text2Color));
            }
        }

        mCountdown = findViewById(R.id.countdown);
        if (needCountdown) {
            mStatusText.setVisibility(View.GONE);
            mCountdown.setVisibility(View.VISIBLE);
            mCountdown.setText("Tap if Done.  Continuing in 5 secs");
            mCountdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            new CountDownTimer(5000, 1000) {
                public void onTick(long millisUntilFinished) {
                    mCountdown.setText("Tap if Done.  Continuing in "+(millisUntilFinished / 1000)+" secs");
                }

                public void onFinish() {
                    mCountdown.setVisibility(View.GONE);
//                    mStatusText.setVisibility(View.VISIBLE);
//                    mBarcodeReaderFragment = attachBarcodeReaderFragment();
                }
            }.start();
//        } else {
//            mStatusText.setVisibility(View.VISIBLE);
//            mCountdown.setVisibility(View.GONE);
//            mBarcodeReaderFragment = attachBarcodeReaderFragment();
        }

        mStatusText.setVisibility(View.VISIBLE);
        mBarcodeReaderFragment = attachBarcodeReaderFragment();

        mChildCount = findViewById(R.id.childCount);
        mChildCount.setText(childScanCountText);
        mChildCount.setVisibility(forChild ? View.VISIBLE : View.GONE);
    }

    private BarcodeReaderFragment attachBarcodeReaderFragment()
    {
        final FragmentManager supportFragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        BarcodeReaderFragment fragment = BarcodeReaderFragment.newInstance(autoFocus, useFlash);
        fragment.setListener(this);
        fragmentTransaction.replace(R.id.fm_container, fragment);
        fragmentTransaction.commitAllowingStateLoss();
        return fragment;
    }

    public static Intent getLaunchIntent(Context context, boolean autoFocus, boolean useFlash, boolean forChild, String scanText, String childScanCount, boolean needCountdown)
    {
        Intent intent = new Intent(context, BarcodeReaderActivity.class);
        intent.putExtra(KEY_AUTO_FOCUS, autoFocus);
        intent.putExtra(KEY_USE_FLASH, useFlash);
        intent.putExtra(KEY_FOR_CHILD, forChild);
        intent.putExtra(KEY_SCAN_TEXT, scanText);
        intent.putExtra(KEY_CHILD_SCAN_COUNT_TEXT, childScanCount);
        intent.putExtra(KEY_NEED_COUNTDOWN, needCountdown);
        return intent;
    }

    @Override
    public void onScanned(Barcode barcode)
    {
        if (mBarcodeReaderFragment != null) {
            mBarcodeReaderFragment.pauseScanning();
        }
        if (barcode != null) {
            Intent intent = new Intent();
            intent.putExtra(KEY_CAPTURED_BARCODE, barcode);
            intent.putExtra(KEY_CAPTURED_RAW_BARCODE, barcode.rawValue);
            setResult(RESULT_OK, intent);

//            if (forChild) {
//                mStatusText.setText("Scan next QR Code");
//            } else
                finish();
        }
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }
}
