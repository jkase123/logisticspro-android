package com.locatorx.lxconnect.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.locatorx.lxconnect.AppServerClient;
import com.locatorx.lxconnect.utils.JSONUtil;
import com.locatorx.lxconnect.MainActivity;
import com.locatorx.lxconnect.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private LinearLayout usernameBack;
    private LinearLayout passwordBack;
    private TextView forgotPassword;
    private TextView copyright;
    private TextView privacy;
    private TextView termsOfUse;
    private LinearLayout termsAndPrivacy;
    private ImageView mLogo;

    private static final String kForgotPassword = "https://lxconnect.locatorx.com/forgotPassword";

    private static final String kPrivacyPolicy = "https://wiki.locatorx.com/privacy-policy";
    private static final String kTermsOfUse = "https://wiki.locatorx.com/terms-of-use";

    private SharedPreferences sharedPreferences;

    private String email, password;
    private String token = null;
    private String appUserId = null;

    private boolean completedAnimation = false;

    private void hideLoginElements(boolean hide)
    {
        usernameBack.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
        passwordBack.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
        loginButton.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
        forgotPassword.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
        copyright.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
        termsAndPrivacy.setVisibility(hide ? View.INVISIBLE : View.VISIBLE);
    }

    private boolean hasCredentials ()
    {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        email = sharedPreferences.getString("email", null);
        password = sharedPreferences.getString("password", null);

        return (email != null && password != null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);

        loginButton = findViewById(R.id.login);
        usernameBack = findViewById(R.id.usernameBack);
        passwordBack = findViewById(R.id.passwordBack);
        forgotPassword = findViewById(R.id.forgotPassword);
        termsAndPrivacy = findViewById(R.id.termsAndPriv);
        copyright = findViewById(R.id.copyright);
        privacy = findViewById(R.id.privacy);
        termsOfUse = findViewById(R.id.termsOfUse);
        mLogo = findViewById(R.id.logo);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String forgotPasswordHtml = sharedPreferences.getString("forgotPassword", kForgotPassword);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(kForgotPassword)));
            }
        });
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String privacyPolicyHtml = sharedPreferences.getString("privacyPolicy", kPrivacyPolicy);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(privacyPolicyHtml)));
            }
        });
        termsOfUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String termsOfUseHtml = sharedPreferences.getString("termsOfUse", kTermsOfUse);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(termsOfUseHtml)));
            }
        });

        getAppSettings();

        if (hasCredentials()) {
            hideLoginElements(true);
            login(email, password);
        } else {
            hideLoginElements(false);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus && !completedAnimation && hasCredentials()) {
            mLogo.bringToFront();
            int w = mLogo.getMeasuredWidth();
            int h = mLogo.getMeasuredHeight();
            int x = mLogo.getLeft();
            int y = mLogo.getTop();
            Rect rectangle = new Rect();
            Window window = getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
            int centerY = rectangle.bottom / 2;

            mLogo.animate()
                    .scaleX(2f)
                    .scaleY(2f)
                    .translationX(x - w)        // the final x = x - (w * scale / 2)
                    .translationY(centerY - h)  // the final y = centerY - (h * scale / 2)
                    .setDuration(1300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLogo.animate()
                                    .alpha(0f)
                                    .setDuration(1300)
                                    .setStartDelay(1300)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            completedAnimation = true;
                                            launchMainActivity();
                                        }
                                    });
                        }
                    });
        }
    }

    private void launchMainActivity()
    {
        if (token != null && completedAnimation) {
            Intent activityIntent = new Intent(this, MainActivity.class);
            activityIntent.putExtra(MainActivity.BUNDLE_TOKEN, token);
            if (appUserId != null)
                activityIntent.putExtra(MainActivity.BUNDLE_APP_USER_ID, appUserId);
            startActivity(activityIntent);

            setResult(Activity.RESULT_OK);

            finish();
        }
    }

    private void getAppSettings()
    {
        new AppSettingsTask().execute();
    }

    private void login(String email, String password)
    {
        new LoginTask().execute(new LoginTaskParams(email, password));
    }

    private void processLoginResults(Map<String, Object> map, String email, String password)
    {
        Map<String, Object> json = JSONUtil.getSafeMap(map, "json");
        Log.i("LoginActivity*****", "json: " + json);
        if (JSONUtil.getSafeBooleanAlt(json, "success")) {
            token = JSONUtil.getSafeString(json, "token");
            appUserId = null;
            Map<String, Object> user = JSONUtil.getSafeMap(json, "user");
            if (user != null)
                appUserId = JSONUtil.getSafeString(user, "appUserId");

            sharedPreferences.edit().putString("email", email)
                    .putString("password", password).commit();

            if (json.containsKey("organization")) {
                Map<String, Object> organization = JSONUtil.getSafeMap(json, "organization");
                JSONUtil.saveJSONData(this, "organization", organization);
            }
            if (json.containsKey("forms")) {
                List<Map<String, Object>> forms = JSONUtil.getSafeMapArray(json, "forms");
                Map<String,Object> formDict = new HashMap<String,Object>();
                for (Map<String,Object> form : forms) {
                    formDict.put(JSONUtil.getSafeString(form, "formId"), form);
                }
                JSONUtil.saveJSONData(this, "forms", formDict);
            }
            if (json.containsKey("actions")) {
                Map<String, Object> actions = JSONUtil.getSafeMap(json, "actions");
                JSONUtil.saveJSONData(this, "actions", actions);
            }
            launchMainActivity();

        } else {
            hideLoginElements(false);
            Toast.makeText(getApplicationContext(), JSONUtil.getSafeString(json, "error"), Toast.LENGTH_SHORT).show();
        }
    }

    private void processAppSettingsResults(Map<String, Object> map)
    {
        if (JSONUtil.getSafeBooleanAlt(map, "success")) {
            Map<String,Object> json = JSONUtil.getSafeMap(map, "json");

            if (json.containsKey("privacyPolicy"))
                sharedPreferences.edit().putString("privacyPolicy", JSONUtil.getSafeString(json, "privacyPolicy"));

            if (json.containsKey("termsOfUse"))
                sharedPreferences.edit().putString("termsOfUse", JSONUtil.getSafeString(json, "termsOfUse"));
        }
    }

    class LoginTaskParams
    {
        String email, password;

        public LoginTaskParams(String email, String password)
        {
            this.email = email;
            this.password = password;
        }
    }

    class LoginTask extends AsyncTask<LoginTaskParams, Void, Map<String, Object>>
    {
        private ProgressDialog pd;
        private LoginTaskParams loginTaskParams;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(LoginActivity.this, "", "Loading", true,
                    false); // Create and show Progress dialog
        }

        @Override
        protected Map<String, Object> doInBackground(LoginTaskParams... params) {

            try {
                loginTaskParams = params[0];
                return AppServerClient.processLogin(loginTaskParams.email, loginTaskParams.password);

            } catch (Exception e) {
                Map<String, Object> errorMap = new HashMap<String, Object>();
                errorMap.put("error", e);
                return errorMap;
            }
        }

        @Override
        protected void onPostExecute(Map<String, Object> map) {
            pd.dismiss();
            try {
                processLoginResults(map, loginTaskParams.email, loginTaskParams.password);
            } catch (Exception e) {}
        }
    }

    class AppSettingsTaskParams
    {
        public AppSettingsTaskParams()
        {
        }
    }

    class AppSettingsTask extends AsyncTask<AppSettingsTaskParams, Void, Map<String, Object>> {
    //    private ProgressDialog pd;
        private AppSettingsTaskParams appSettingsTaskParams;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
    //        pd = ProgressDialog.show(LoginActivity.this, "", "Loading", true,
    //                false); // Create and show Progress dialog
        }

        @Override
        protected Map<String, Object> doInBackground(AppSettingsTaskParams... params) {

            try {
                return AppServerClient.processAppSettings();

            } catch (Exception e) {
                Map<String, Object> errorMap = new HashMap<String, Object>();
                errorMap.put("error", e);
                return errorMap;
            }
        }

        @Override
        protected void onPostExecute(Map<String, Object> map) {
    //        pd.dismiss();
            try {
                processAppSettingsResults(map);
            } catch (Exception e) {}
        }
    }

}
